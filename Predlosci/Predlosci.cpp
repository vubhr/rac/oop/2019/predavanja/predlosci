#include <iostream>

template <typename T>
T zbroji(T a, T b) {
	return a + b;
}

int main() {
	std::cout << zbroji<double>(2, 3) << std::endl;
	std::cout << zbroji(2.2, 3.2) << std::endl;
}
