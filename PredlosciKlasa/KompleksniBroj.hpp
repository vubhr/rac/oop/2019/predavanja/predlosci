template <typename T>
class KompleksniBroj {
public:
	KompleksniBroj(T realni, T imaginarni) {
		this->realni = realni;
		this->imaginarni = imaginarni;
	}
	T getRealni() {
		return realni;
	}
private:
	T realni;
	T imaginarni;
};