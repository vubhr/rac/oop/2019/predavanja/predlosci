#pragma once

class Matematika {
public:
	template <typename T>
	static T zbroji(T a, T b) {
		return a + b;
	}
	template <typename T>
	static T oduzmi(T a, T b) {
		return a - b;
	}
private:
};